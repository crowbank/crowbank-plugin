<?php
// Sample array

$base_url='http://178.62.196.5/api/check_availability';
$queryString = $_SERVER['QUERY_STRING'];
$url = $base_url . '?' . $queryString;

//echo $url;
//exit();
$contents = file_get_contents($url);
$payload = json_decode($contents);
$data = $payload->{'data'};
$avail = $data->{'overall_availability'};

$booking_link = '<a href="crowbank.co.uk/booking">booking form</a>';

//If $contents is not a boolean FALSE value.
if($contents !== false){
    //Print out the contents.
    if ($avail == 1) {
        $html = '<h2>Limited Availability</h2>';
        $html .= '<p>We have limited availablity for the period.<br>';
        $html .= 'Please use our ' . $booking_link;
        $html .= ' to enquire and get an accurate response.</p>';
    } else if ($avail == 0) {
        $html = '<h2>Good News!</h2>';
        $html .= '<p>We have good availability for the dates you selected.<br>';
        $html .= 'Please use our ' . $booking_link;
        $html .= ' to make your booking</p>';
    } else {
        $html = '<h2>Sorry...</h2>';
        $html .= "<p>Unfortunately, we don't currently have availability for ";
        $html .= 'your dates';
    }
} else {
    $html = '<h3>Error</h3>';
    $html .= '<p>Our server appears to be offline - please try again later</p>';
}

$data = array("html" => $html);

header("Content-Type: application/json");
echo json_encode($data);
exit();