from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest
from datetime import datetime
import mysql.connector
from pymongo import MongoClient

CHROME_DRIVER = r'C:\Program Files\chromedriver.exe'

class MySQL:
    mydb = None
    
    @classmethod
    def get_cursor(cls):
        if not cls.mydb:
            cls.mydb = mysql.connector.connect(host="localhost", user="crowbank", passwd="Crowbank454")
        return cls.mydb.cursor()
        
class DataClass:
    def __init__(self, _data):
        self._data = _data
    
    def __getattr__(self, name):
        if name in self._data:
            return self._data[name]
        raise AttributeError

class CollectionDataClass(DataClass):
    _items = {}
    
    @classmethod
    def get(cls, name):
        if name in cls._items:
            return cls._items[name]
        db = MongoClient().pytest
        col = db.get_collection(cls.class_name)
        _data = col.find_one({'name': name})
        item = cls(_data)
        cls._items[name] = item
        return item
    

class Login(CollectionDataClass):
    class_name = 'login'

    def login(self, driver):
        pass


class Page(CollectionDataClass):
    class_name = 'page'
      
    def __len__(self):
        return len(self._data['elements'])
        
    def __getitem__(self, key):
        return self._data['elements'][key]
    
    def __iter__(self):
        return iter(self._data['elements'])
    
    def find(self, driver, name):
        if name in self._data['elements']:
            return driver.find_element_by_xpath(self._data['elements'][name]['xpath'])


class Script(CollectionDataClass):
    class_name = 'script'
        
    def __init__(self, _data):
        super().__init__(_data)
        self.steps = []
        for step in _data['steps']:
            self.steps.append(Step(self, step))

    def __len__(self):
        return len(self.steps)
    
    def __iter__(self):
        return iter(self.steps)

    def action(self, driver):
        for step in self.steps:
            print(step)
            step.action(driver)


class Step(DataClass):
    def __init__(self, script, _data):
        super().__init__(_data)
        self.script = script
        if 'page' in _data:
            self.page = Page.get(_data['page'])
        
    def __repr__(self):
        s = f"{self.n}: {self.type} "
        if self.type == 'message':
            s += f"{self.message['type']} ({self.message['status']}): {self.message['content']}"
            return s
        x = self.expected()
        if self.type == 'attr':
            s += self.attr + ' '
        if self.type in ('text', 'attr'):
            s += f"of {self.element} in {self.page.name} is {x}"
        elif self.type == 'send_keys':
            s += f"{x} to {self.element} in {self.page.name}"
        elif self.type == 'click':
            s += f"on {self.element} in {self.page.name}"
        elif self.type == 'submit':
            s += f"{self.element} in {self.page.name}"
        elif self.type == 'get':
            s += self.page.name
        return s
    
    def expected(self):
        x = self.value if 'value' in self._data else ''
        if x == '%username':
            login_name = self.script.login
            login = Login.get(login_name)
            return login.username
        elif x == '%password':
            login_name = self.script.login
            login = Login.get(login_name)
            return login.password
        elif x == '%today':
            return datetime.now().strftime('%d/%m/%Y')
        return x
        
    def action(self, driver):
        if self.type == 'get':
            driver.get(self.page.url)
            return
        
        if self.type == 'message':
            assert 'message' in self._data
            message = self.message
            check_message(message)
            return
        
        elem = self.page.find(driver, self.element)
        assert elem
        
        x = self.expected()
        
        if self.type in ('text', 'attr'):
            actual = ''
            
            if self.type == 'text':
                actual = elem.text
            elif self.type == 'attr':
                actual = elem.get_attribute(self.attr)              

            if x and actual and actual != x:
                print(f"Error in step {self.n} of {self.script.name} checking {self.element} in {self.page.name}: actual {actual}, expected {self.expected()}")
                assert False

        if self.type == 'send_keys':
            elem.send_keys(x)
        elif self.type == 'submit':
            elem.submit()
        elif self.type == 'click':
            elem.click()


def clean_messages():
    # Remove all open messages
    cur = MySQL.get_cursor()
    sql = """delete
from crowbank_wordpress.crwbnk_msgmeta
where msg_id IN (
    select distinct msg_id
    from crowbank_wordpress.crwbnk_messages
    where msg_status = 'open'
)"""

    cur.execute(sql)
    sql = """delete
from crowbank_wordpress.crwbnk_messages
where msg_status = 'open'"""

    cur.execute(sql)
    
def clean_bookings():
    # Remove all draft bookings
    cur = MySQL.get_cursor()
    sql = """delete
from crowbank_petadmin.my_bookingitem
where bi_bk_no in (
    select bk_no
    from crowbank_petadmin.my_booking
    where bk_status = 'D'
)"""
    cur.execute(sql)

    sql = "delete from crowbank_petadmin.my_booking where bk_status = 'D'"
    cur.execute(sql)


def get_driver():
    driver = webdriver.Chrome(CHROME_DRIVER)
    driver.implicitly_wait(10)
    return driver

def check_message(message):
    cur = get_mysql()
    assert 'type' in message
    assert 'status' in message
    assert 'content' in message
    
    cur.execute(f"select max(msg_id) from crowbank_wordpress.crwbnk_messages where msg_type = {message['type']} and msg_status = {message['status']}")
    msg_id = cur.fetchone[0]
    assert msg_id
    expected = message['content']
    actual = {}
    cur.execute(f"select meta_kind, meta_value from crowbank_wordpress.crwbnk_msgmeta where msg_id = {msg_id}")
    res = cur.fetchall()
    for (actual_kind, actual_value) in res:
        actual[actual_kind] = actual_value

    for (k, v) in expected:
        assert k in actual
        assert v == actual[k]
            
    cur.close()

@pytest.fixture(scope="session")
def login():
    def _login(name):
        return Login.get('Login-' + name)
    
@pytest.fixture(scope="session")
def driver(request):
    driver = get_driver()
    yield driver
    driver.close()

# @pytest.mark.dependency()
# def test_login(driver, name):
#     driver.get("http://localhost/wordpress")
#     user_login = login(1)
#     home_values = VALUES['home']
#     home_values.check(driver)
#     home_values.fill(driver)
#     user_login.fill(driver)
#     customer = VALUES['customer']
#     customer.check(driver)

# @pytest.mark.dependency(depends=["test_login"])
# def test_new_booking(driver):
#    check_form(driver, VALUES['customer'], VALUES['booking'])
    
# @pytest.mark.dependency(depends=["test_new_booking"])
def run_script(driver, name):
    script = Script.get(name)
    assert script

    script.action(driver)    
    
# @pytest.mark.dependency(depends=["test_new_booking"])
# def test_booking_confirmed(driver):
#    check_form(driver, VALUES['booking_confirmation_new'], None)

def main():
    clean_messages()
    clean_bookings()
    driver = get_driver()
    run_script(driver, "Simple Booking")
    driver.close()
    
if __name__ == '__main__':
    main()