<?php

defined( 'ABSPATH' ) || die( 'No direct script access allowed!' );
define( 'CROWBANK_ABSPATH', plugin_dir_path( __FILE__ ) );

crowbank_shortcodes_init();
$availability = new Availability();

$availability->load();

$date = $_GET['date'];
$species = $_GET['species'];
$run_type = $_GET['run_type'];

$aa = $availability->availability($date, $species, $run_type);

echo json_encode($aa);
?>
