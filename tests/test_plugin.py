from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest
from datetime import datetime
import mysql.connector
import pymssql

CHROME_DRIVER = r'C:\Program Files\chromedriver.exe'

class Login:
    _logins = {}
    _loaded = False
    
    @classmethod
    def load(cls):
        cur = get_sqlsrv()
        sql = "select login_username, login_password, login_name, login_desc from tbllogin"
        cur.execute(sql)
        for row in cur.fetchall():
            username = row[0]
            passwd = row[1]
            name = row[2]
            desc = row[3]
            cls._logins[name] = Login(username, passwd, name, desc)
    
    @classmethod
    def get(cls, name):
        if not cls._loaded:
            cls.load()
        assert name in cls._logins
        return cls._logins[name]
    
    def __init__(self, username, password, name, desc):
        self.username = username
        self.password = password
        self.name = name
        self.desc = desc
    
    def login(self, driver):
        pass

class Form:
    _forms = {}
    _loaded = False
    
    @classmethod
    def load(cls):
        cur = get_sqlsrv()
        sql = "select ff_form_name, ff_form_url, ff_name, ff_xpath from vwformfield"
        cur.execute(sql)
        for row in cur.fetchall():
            form_name = row[0]
            if not form_name in cls._forms:
                cls._forms[form_name] = Form(form_name, row[1], {})
            cls._forms[form_name].xpaths[row[2]] = row[3]
        cls._loaded = True
    
    @classmethod
    def get(cls, name):
        if not cls._loaded:
            cls.load()
        if name in cls._forms:
            return cls._forms[name]
        
    def __init__(self, name, url, xpaths):
        self.name = name
        self.url = url
        self.xpaths = xpaths
    
    def find(self, driver, name):
        if not self._loaded:
            self.load()
        if name in self.xpaths:
            return driver.find_element_by_xpath(self.xpaths[name])


class Value:
    def __init__(self, value_set, field_name, action, value, attr = ''):
        self.value_set = value_set
        self.form = value_set.form
        self.field_name = field_name
        self.action = action
        self.value = value
        self.attr = attr

    def check(self, driver):
        actual = ''
        elem = self.form.find(driver, self.field_name)
        if not elem:
            print(f"Element name {self.field_name} not found in form {self.form.name}")
        
        if self.action == 'text':
            actual = elem.text
        elif self.action == 'attr':
            actual = elem.get_attribute(self.attr)
        
        if actual != self.value:
            print(f"Error checking {self.field_name} in {self.form.name}: actual {actual}, expected {self.value}")
            assert False

    def fill(self, driver):
        elem = self.form.find(driver, self.field_name)
        if self.action == 'send_keys':
            elem.send_keys(self.value)
        elif self.action == 'submit':
            elem.submit()
        elif self.action == 'click':
            elem.click()

        
class Values:
    _value_sets = {}
    _loaded = False
    
    @classmethod
    def load(cls):
        cur = get_sqlsrv()
        sql = "select fv_fvs_name, fv_form_name, fv_ff_name, fv_action, fv_attr, fv_value from vwformvalue"
        cur.execute(sql)
        for row in cur.fetchall():
            values_name = row[0]
            field_name = row[2]
            action = row[3]
            attr = row[4]
            value = row[5]
            if not values_name in cls._value_sets:
                form_name = row[1]
                form = Form.get(form_name)
                if not form:
                    print(f"Unable to find form {form_name}")
                    assert False
                values = Values(values_name, form, {})
                cls._value_sets[values_name] = values
            else:
                values = cls._value_sets[values_name]
            values.values[field_name] = Value(values, field_name, action, value, attr) 
    
    @classmethod
    def get(cls, no):
        if not cls._loaded:
            cls.load()
        if no in cls._value_sets:
            return cls._value_sets[no]
    
    def __init__(self, name, form, values):
        self.name = name
        self.form = form
        for v in values.values():
            if type(v) == tuple:
                if not v[0] in ('title', 'text', 'attr', 'send_keys', 'click', 'submit'):
                    print(f"Unknown value of type {v[0]}")
                    assert False
            elif type(v) != str:
                print(f"Unknown value {v}")
                assert False
        self.values = values

    def check(self, driver):
        for value in self.values:
            value.check(driver)
            
    def fill(self, driver):
        for value in self.values:
            value.fill(driver)
        
mydb = None
sqlsrv = None

def get_driver():
    driver = webdriver.Chrome(CHROME_DRIVER)
    driver.implicitly_wait(10)
    return driver

def get_mysql():
    if not mydb:
        mydb = mysql.connector.connect(host="localhost", user="crowbank", passwd="Crowbank454")
    return mydb.cursor()

def get_sqlsrv():
    if not sqlsrv:
        sqlsrv = pymssql.connect(server="localhost", user="PA",
                                 password="petadmin", database="pytest")
    return sqlsrv.cursor()

def check_form(driver, action, values):
    if action:
        action.fill(driver)
    if values:
        values.check(driver)

def check_message(msg_type, msg_status, msg_content):
    cur = get_mysql()
    cur.execute(f"select max(msg_id) from crowbank_wordpress.crwbnk_messages where msg_type = {msg_type} and msg_sdtatus = {msg_status}")
    msg_id = cur.fetchone[0]
    assert msg_id
    cur.execute(f"select meta_kind, meta_value from crowbank_wordpress.crwbnk_msgmeta where msg_id = {msg_id}")
    res = cur.fetchall()
    for (actual_kind, actual_value) in res:
        if actual_kind in msg_content:
            assert actual_value == msg_content[actual_kind]
            
    cur.close()

@pytest.fixture(scope="session")
def login():
    def _login(name):
        return Login.get('Login-' + name)
    
@pytest.fixture(scope="session")
def driver(request):
    driver = get_driver()
    yield driver
    driver.close()

# @pytest.mark.dependency()
# def test_login(driver, name):
#     driver.get("http://localhost/wordpress")
#     user_login = login(1)
#     home_values = VALUES['home']
#     home_values.check(driver)
#     home_values.fill(driver)
#     user_login.fill(driver)
#     customer = VALUES['customer']
#     customer.check(driver)

# @pytest.mark.dependency(depends=["test_login"])
# def test_new_booking(driver):
#    check_form(driver, VALUES['customer'], VALUES['booking'])
    
@pytest.mark.dependency(depends=["test_new_booking"])
def test_make_booking(driver):
    check_form(driver, VALUES['booking_new'], VALUES['booking_confirmation'])

@pytest.mark.dependency(depends=["test_new_booking"])
def test_booking_confirmed(driver):
    check_form(driver, VALUES['booking_confirmation_new'], None)


def main():
    driver = get_driver()
    test_login(driver, 1)
    test_new_booking(driver)
    test_make_booking(driver)
    test_booking_confirmed(driver)
    driver.close()
    
if __name__ == '__main__':
    main()