from pymongo import MongoClient
import pymssql

def import_login():
    client = MongoClient()
    testdb = client.pytest
    sqlsrv = pymssql.connect(server=r"localhost\sqlexpress", user="PA", password="petadmin", database="pytest")
    cur = sqlsrv.cursor()

    col = testdb.login
    sql = "select login_username, login_password, login_name, login_desc, login_cust_no from tbllogin"
    cur.execute(sql)
    logins = {}
    for row in cur.fetchall():
        d = {
            '_id': row[4],    
            'username': row[0],
            'password': row[1],
            'name': row[2],
            'desc': row[3],
            'cust_no': row[4],
            'pets': []
        }
        logins[d['username']] = d

    sql = """select login_username, pet_name, spec_desc, breed_desc
    from tbllogin
    join pa..tblpet on pet_cust_no = login_cust_no
    join pa..tblspecies on pet_spec_no = spec_no
    join pa..tblbreed on breed_no = pet_breed_no
    where pet_deceased = 'N'
    """
        
    cur.execute(sql)
    for row in cur.fetchall():
        pet = {
            'name': row[1],
            'species': row[2],
            'breed': row[3]
        }
        logins[row[0]]['pets'].append(pet)

    for login in logins.values():
        col.insert_one(login)

def import_page():
    client = MongoClient()
    testdb = client.pytest
    sqlsrv = pymssql.connect(server=r"localhost\sqlexpress", user="PA", password="petadmin", database="pytest")
    cur = sqlsrv.cursor()
    col = testdb.page
    pages = {}
    sql = "select form_name, form_url, form_desc from tblform"
    cur.execute(sql)
    for row in cur.fetchall():
        d = {
            'name': row[0],
            'desc': row[2],
            'elements': {}
        }
        if row[1] != '':
            d['url'] = row[1]
        pages[row[0]] = d

    sql = """select form_name, ff_name, ff_desc, ff_xpath
    from tblformfield
    join tblform on ff_form_no = form_no"""

    cur.execute(sql)
    for row in cur.fetchall():
        page = row[0]
        name = row[1]
        desc = row[2]
        xpath = row[3]
        pages[page]['elements'][name] = {
            'page': page,
            'name': name,
            'desc': desc,
            'xpath': xpath
        }

    for page in pages.values():
        col.insert_one(page)

def read_page(page_name):
    col = MongoClient().pytest.page
    page = col.find_one({'name': page_name})
    for (k, v) in page.items():
        if k == 'elements':
            for ev in v.values():
                print(ev['name'], ev['xpath'])
        else:
            print(k, v)

def import_script():
    client = MongoClient()
    testdb = client.pytest
    sqlsrv = pymssql.connect(server=r"localhost\sqlexpress", user="PA", password="petadmin", database="pytest")
    cur = sqlsrv.cursor()
    col = testdb.script
    
    sql = """select fv_fvs_name, fv_fvs_desc, fv_ff_name, fv_form_name, fv_action, fv_attr, fv_value
from vwformvalue"""

    sets = {}
    cur.execute(sql)
    for row in cur.fetchall():
        set_name = row[0]
        set_desc = row[1]
        element_name = row[2]
        page_name = row[3]
        action = row[4]
        attr = row[5]
        value = row[6]
        if not set_name in sets:
            sets[set_name] = {
                'name': set_name,
                'desc': set_desc,
                'page_name': page_name,
                'elements': []
            }
        element = {
            'name': element_name,
            'action': action,
            'value': value
        }
        if attr != '':
            element['attr'] = attr
        sets[set_name]['elements'].append(element)
    
    m = {
        'name': 'Mindy-1',
        'desc': 'A simple booking for booking, with ample availability',
        'steps': [
            sets['Home'],
            sets['Customer'],
            sets['Booking-Mindy-0'],
            sets['BookingConfirmation-Mindy-0'],
            sets['Booking-Mindy-1']
        ]
    }

    col.insert_one(m)

def convert_script():
    old_col = MongoClient().pytest.script_old
    new_col = MongoClient().pytest.script
    
    
    pass

convert_script()