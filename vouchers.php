<?php

add_filter( 'gform_field_validation_19_18', 'amount_validation', 10, 4 );

function amount_validation( $result, $value, $form, $field  ) {
    if ( $result['is_valid'] && floatval( $value ) == 0 ) {
        $result['is_valid'] = false;
        $result['message'] = 'Please enter a number without a pound sign';
    }
    return $result;
}

function voucher_confirmation( $confirmation, $form, $entry) {
    global $petadmin;
    
    GFCommon::log_debug( 'Entered voucher_confirmation');
        
    $email = rgar( $entry, '1' );
    $existing_customer = rgar( $entry, '2.1' );
    
    GFCommon::log_debug( 'Found existing_customer = ' . $existing_customer );
    
    $amount = floatval( rgar( $entry, '18') ) * 0.95;

    $entry_id = rgar( $entry, 'id');
    GFCommon::log_debug( 'entry_id = ' . $entry_id );

    $customer = $petadmin->customers->get_by_email($email);

    if ($existing_customer && $customer) {
        $name = $customer->display_name();
        $telno = $customer->main_telno();

        $addr1 = $customer->addr1;
        $town = $customer->addr3;
        $postcode = $customer->postcode;
    } elseif (!$existing_customer && !$customer) {
        $name = rgar( $entry, '16') . ' ' . rgar($entry, '6');
        $telno = rgar( $entry, '12');
        if (!$telno) {
            $telno = rgar( $entry, '13');
        }

        $addr1 = rgar( $entry, '8.1');
        $town = rgar( $entry, '8.3');
        $postcode = rgar( $entry, '8.5');
    } elseif ($existing_customer) {
       $confirmation = 'Unable to locate your email; please enter an email address previously used to confirm your Crowbank bookings, or check New Customer box';
       return $confirmation;
    } else {
        $confirmation = 'Found your email associated with an existing customer. Please return and check Existing Customer box';
        return $confirmation; 
    }

    $url = 'https://secure.worldpay.com/wcc/purchase?instId=1094566&';
    $url .= 'cartId=PBL-V' . $entry_id;
    $url .= '&amount=' . $amount . '&currency=GBP&';
    $url .= 'desc=Payment+for+Crowbank+Boarding+Voucher+' . urlencode('#') . $entry_id;
    $url .= '&name=' . urlencode($name);
    $url .= '&email=' . urlencode($email);
    $url .= '&address1=' . urlencode($addr1);
    $url .= '&town=' . urlencode($town);
    $url .= '&postcode=' . urlencode($postcode);
    $url .= '&country=UK';

    if ($telno) {
        $url .= '&tel=' . urlencode($telno);
    }

    $confirmation = array( 'redirect' => $url );

    return $confirmation;
}

GFCommon::log_debug( 'Adding voucher_confirmation as filter');
add_filter( 'gform_confirmation_19', 'voucher_confirmation', 10, 4 );