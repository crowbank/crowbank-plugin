<?php
require_once CROWBANK_ABSPATH . "classes/calendar_class.php";
require_once CROWBANK_ABSPATH . "classes/availability_class.php";

function crowbank_calendar_legend($attr = [], $content = null, $tag = '') {		
	$attr = array_change_key_case((array)$attr, CASE_LOWER);
	
	$attr = shortcode_atts([ 'class' => ''], $attr, $tag);
	
	$class = $attr['class'];

	$r = '<h1>Legend</h1>';
    $r .= '<div class="legend">';
    $r .= '		<div class="legend-card">';
    $r .= '			<div class="legend-text"><h2>Good Availability</h2></div>';
    $r .= '				<div class="legend-square free">';
	$r .= '                <div class="legend-am">am</div>';
	$r .= '                <div class="legend-pm">pm</div>';
	$r .= '            </div>';
	$r .= '        <div class="legend-detail">';
	$r .= '            <p>We have <strong>good</strong> availability for the dates.</p>';
	$r .= '            <p>Please note that availability can change quickly, so make your booking as soon as possible.</p>';
	$r .= '        </div>';
	$r .= '    </div>';
	$r .= '    <div class="legend-card">';
	$r .= '        <div class="legend-text"><h2>Limited Availability</h2></div>';
	$r .= '        <div class="legend-square busy">';
	$r .= '            <div class="legend-am">am</div>';
	$r .= '            <div class="legend-pm">pm</div>';
	$r .= '        </div>';
	$r .= '        <div class="legend-detail">';
	$r .= '                <p>We have <strong>limited</strong> availability for the dates.</p>';
	$r .= '                <p>Please get in touch before finalizing your plans.</p>';
	$r .= '            </div>';
	$r .= '        </div>';
	$r .= '    <div class="legend-card">';
	$r .= '        <div class="legend-text"><h2>No Availability</h2></div>';
	$r .= '        <div class="legend-square full">';
	$r .= '            <div class="legend-am">am</div>';
	$r .= '            <div class="legend-pm">pm</div>';
	$r .= '        </div>';
	$r .= '        <div class="legend-detail">';
	$r .= '                <p>We have <strong>no</strong> availability for the dates.</p>';
	$r .= '                <p>Please let us know if you want to set up a standby booking.</p>';
	$r .= '            </div>';
	$r .= '        </div>';
	$r .= '    </div>';
	$r .= '</div>';

	$rr = '<div' . ($class == '' ? '' : ' class="' . $class . '"') . '>';
	
	$r = $rr . $r . '</div>';
	
	return $r;
}
add_shortcode('crowbank_calendar_legend', 'crowbank_calendar_legend');

function crowbank_load_calendar($runtype, $offset) {
	global $petadmin_db;
	
	$html = crowbank_calendar(['offset' => $offset, 'runtype' => $runtype], null, '', 1);
	$sql = "insert into crwbnk_calendar_cache (runtype, offset, html) values ('" . $runtype . "', " . $offset . ", '" . $html . "')";
	$petadmin_db->execute($sql);
}

function crowbank_load_calendars () {
	global $petadmin_db;
	
	$sql = 'truncate table crwbnk_calendar_cache';
	$petadmin_db->execute($sql);
	
	for ($i = 0; $i < 12; $i ++) {
		crowbank_load_calendar('kennels', $i);
		crowbank_load_calendar('deluxe', $i);
		crowbank_load_calendar('cattery', $i);
	}
	return 'calendars loaded<br>';
}
add_shortcode('crowbank_load_calendars', 'crowbank_load_calendars');

function crowbank_calendar($attr = [], $content = null, $tag = '', $force = 1) {
	global $petadmin;
	global $petadmin_db;
	
	$months = array (1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
			7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
	
	$availability = new Availability();
	$availability->load();
	
	$attr = array_change_key_case((array)$attr, CASE_LOWER);
	
	$attr = shortcode_atts([ 'class' => '', 'title' => '', 'offset' => 0, 'runtype' => 'kennels'], $attr, $tag);

	$title = $attr['title'];
	$class = $attr['class'];
	$offset = $attr['offset'];
	$runtype = $attr['runtype'];
	$availabilityClasses = array();
	$availabilityClasses[0] = 'free';
	$availabilityClasses[1] = 'free_busy';
	$availabilityClasses[2] = 'free_full';
	$availabilityClasses[10] = 'busy_free';
	$availabilityClasses[11] = 'busy';
	$availabilityClasses[12] = 'busy_full';
	$availabilityClasses[20] = 'full_free';
	$availabilityClasses[21] = 'full_busy';
	$availabilityClasses[22] = 'full';
	
	if ($runtype == 'cattery') {
		$species = 'Cat';
		$run_type = 'Cat';
	} else {
		$species = 'Dog';
		if ($runtype == 'kennels')
			$run_type = 'Any';
		else
			$run_type = 'Deluxe';
	}
	
	if ($force == 0) {
		/* read from database, rather than evaluate */
		
		$sql = "select html from crwbnk_calendar_cache where runtype = '" . $runtype . "' and offset = " . $offset;
		$result = $petadmin_db->execute($sql);
		foreach($result as $row) {
			$r = $row['html'];
		}
	} else {
		$classFunc = function($date) use ($availability, $species, $run_type, $availabilityClasses) {
			$aa = $availability->availability($date, $species, $run_type);
			$a = 10 * $aa['am'] + $aa['pm'];
			if (isset($availabilityClasses[$a]))
				return $availabilityClasses[$a];
			
			return '';
		};
		
		if (isset($_REQUEST['monthyear']))
			$monthyear = $_REQUEST['monthyear'];
		else
			$monthyear = 0;
		
		$month = intval(date("m", time()));
		$year = intval(date("Y", time()));
		
		$offset += $monthyear;
		
		if ($offset > 0) {
			$month += $offset;
			while ($month > 12) {
				$year += 1;
				$month -= 12;
			}
		}
		
		$calendar = new Calendar();
		$calendar->currentYear = $year;
		$calendar->currentMonth = $month;
		$calendar->classFunc = $classFunc;
		
		if (!$title) {
			$title = $months[$month] . ' ' . $year;
		}
		
		$calendar->title = $title;
		
		$r = $calendar->show();
	
		$rr = '<div' . ($class == '' ? '' : ' class="' . $class . '"') . '>';
		
		$r = $rr . $r . '</div>';
	}
	return $r;
}
add_shortcode('crowbank_calendar', 'crowbank_calendar');

function test_row($date, $species, $runtype, $availability) {
	global $petadmin_db;

	$sql = "select ra_availability_am, ra_availability_pm from my_availability where ra_date = '" . $date->format('Y-m-d') . "' and ra_spec = '" . $species . "' and ra_rt_desc = '" . $runtype . "'";
		
	$result = $petadmin_db->execute($sql);
	foreach ($result as $row) {
		$availability_am = (int) $row['ra_availability_am'];
		$availability_pm = (int) $row['ra_availability_pm'];
		break;
	}
	
	$da = $availability_am + 10 * $availability_pm;

	$aa = $availability->availability($date, $species, $runtype);
	$a = 10 * $aa['am'] + $aa['pm'];

	$r = '<tr>';
	$r.= '<td>' . $date->format('d/m/Y') . '</td>';
	$r.= '<td>' . $species . '</td>';
	$r.= '<td>' . $runtype . '</td>';
	$r.= '<td>' . $a . '</td>';
	$r.= '<td>' . $da . '</td>';
	$r.= '</tr>';
	return $r;
}

function test_availability() {
	global $petadmin_db;

	$availability = new Availability();
	$availability->load();

	$r = '<table>';
	$r .= test_row(new DateTime('2024-11-03'), 'Dog', 'Any', $availability);
	$r .= test_row(new DateTime('2024-11-17'), 'Dog', 'Any', $availability);
	$r .= test_row(new DateTime('2024-11-17'), 'Dog', 'Deluxe', $availability);
	$r .= test_row(new DateTime('2024-11-24'), 'Dog', 'Any', $availability);

	$r .= '</table>';
	return $r;
	/* Create an html table showing availability for 03/11/24, 17/11/24 and 26/11/24 */
	$a = $availability->availability(new DateTime('2024-11-03'), 'Dog', 'Any')['am'];
	$r .= '2024-11-03 Dog Any ' . $a . '<br>';
	$a = $availability->availability(new DateTime('2024-11-03'), 'Dog', 'Deluxe')['am'];
	$r .= '2024-11-03 Dog Deluxe ' . $a . '<br>';
	$a = $availability->availability(new DateTime('2024-11-03'), 'Dog', 'Cattery')['am'];
	$r .= '2024-11-03 Dog Cattery ' . $a . '<br>';
	$a = $availability->availability(new DateTime('2024-11-03'), 'Cat', 'Any')['am'];
	$r .= '2024-11-03 Cat Any ' . $a . '<br>';
	$a = $availability->availability(new DateTime('2024-11-03'), 'Cat', 'Deluxe')['am'];
	$r .= '2024-11-03 Cat Deluxe ' . $a . '<br>';


	$a = $availability->availability(new DateTime('2024-11-17'), 'Dog', 'Any')['am'];

	return $a;

	$r = '';
	$sql = "select ra_date, ra_spec, ra_rt_desc, ra_availability_am, ra_availability_pm from my_availability where ra_date = '2024-11-17'";

	$result = $petadmin_db->execute($sql);
	foreach ($result as $row) {
		$date = new DateTime($row['ra_date']);
		$species = $row['ra_spec'];
		$rt_desc = $row['ra_rt_desc'];
		$availability_am = (int) $row['ra_availability_am'];
		$availability_pm = (int) $row['ra_availability_pm'];

		$r .= $date->format('Y-m-d') . " $species $rt_desc $availability_am $availability_pm<br>";
	}
	return $r;
}

function crowbank_calendar_test($attr = [], $content = null, $tag = '', $force = 1) {
	return test_availability();


	global $petadmin;
	global $petadmin_db;
	
	$months = array (1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
			7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
	
	$availability = new Availability();
	$availability->load();
	
	$attr = array_change_key_case((array)$attr, CASE_LOWER);
	
	$attr = shortcode_atts([ 'class' => '', 'title' => '', 'offset' => 0, 'runtype' => 'kennels'], $attr, $tag);

	$title = $attr['title'];
	$class = $attr['class'];
	$offset = $attr['offset'];
	$runtype = $attr['runtype'];
	$availabilityClasses = array();
	$availabilityClasses[0] = 'free';
	$availabilityClasses[1] = 'free_busy';
	$availabilityClasses[2] = 'free_full';
	$availabilityClasses[10] = 'busy_free';
	$availabilityClasses[11] = 'busy';
	$availabilityClasses[12] = 'busy_full';
	$availabilityClasses[20] = 'full_free';
	$availabilityClasses[21] = 'full_busy';
	$availabilityClasses[22] = 'full';
	
	$title = $attr['title'];
	$class = $attr['class'];
	$offset = $attr['offset'];
	$runtype = 'kennels';
	$offset = 5;

	$species = 'Dog';
	$run_type = 'Any';

	/* set $date to be a php Date object equal to 11/17/2024 */
	$date = new DateTime('2024-11-17');	
	$aa = $availability->availability($date, $species, $run_type);
	
	/* return a display version of $aa */
	$a = 10 * $aa['am'] + $aa['pm'];
	$avail = $availabilityClasses[$a];
	return $avail;

	$classFunc = function($date) use ($availability, $species, $run_type, $availabilityClasses) {
			$aa = $availability->availability($date, $species, $run_type);
			$a = 10 * $aa['am'] + $aa['pm'];
			if (isset($availabilityClasses[$a]))
				return $availabilityClasses[$a];
			
			return '';
		};
		
		if (isset($_REQUEST['monthyear']))
			$monthyear = $_REQUEST['monthyear'];
		else
			$monthyear = 0;
		
		$month = intval(date("m", time()));
		$year = intval(date("Y", time()));
		
		$offset += $monthyear;
		
		if ($offset > 0) {
			$month += $offset;
			while ($month > 12) {
				$year += 1;
				$month -= 12;
			}
		}
		
		$calendar = new Calendar();
		$calendar->currentYear = $year;
		$calendar->currentMonth = $month;
		$calendar->classFunc = $classFunc;
		
		if (!$title) {
			$title = $months[$month] . ' ' . $year;
		}
		
		$calendar->title = $title;
		
		$r = $calendar->show();
	
		$rr = '<div' . ($class == '' ? '' : ' class="' . $class . '"') . '>';
		
		$r = $rr . $r . '</div>';
	return $r;
}
add_shortcode('crowbank_calendar_test', 'crowbank_calendar_test');
